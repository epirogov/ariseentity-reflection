using System;
using PluginInterface;

namespace EntityReflection
{
	
	public class ClassA : PluginInterface.BaseInterface
	{
		
		public ClassA()
		{
		}
		
		public void Method()
		{
			Console.WriteLine(this.GetType().Name + " Class A method");			
		}
		
//		public void SecondMethod()
//		{
//		}
		
		public bool Identify(object key)
		{
			return (key == fKey);
			
		}
		
		private object fKey = "ClassA";
		
//		public virtual object GetKey()
//		{
//			return fKey;
//		}
		
		public object Key
		{
			get
			{
				return fKey;
			}
			
			set
			{
				fKey = value;
			}
		}
	}
	
}
