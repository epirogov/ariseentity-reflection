using System;
using System.Reflection;
using System.Reflection.Emit;
using PluginInterface;
using EntityReflection;

namespace EntityReflection
{

	public delegate void InnerAction ();
	
	public class DecorateBehavior : IDecorateBehaviour, BaseInterface
	{
		
		public DecorateBehavior()
		{
		}
		
		private void CreateWriteLineMessage(MethodBuilder decoratedMethodBuilder, String message)
		{
		    
		    ILGenerator ilGeneratorForMethod =  decoratedMethodBuilder.GetILGenerator();		    
		    		    
		    //ilGeneratorForMethod.Emit(OpCodes.Ldstr, message);
		    //ilGeneratorForMethod.Emit(OpCodes.Ldc_I4_0);
		    		    
		    //ilGeneratorForMethod.Emit(OpCodes.Ldarg_2);		    
		    		    
		    //MethodInfo writeLineMethodInfo = typeof(Console).GetMethod("WriteLine");		    
		    //ilGeneratorForMethod.EmitCall(OpCodes.Call, writeLineMethodInfo, new Type [] {typeof(String)});
		    
		    ilGeneratorForMethod.EmitWriteLine(message);
		    
		}
		
		public void PrefixAction(MethodBuilder decoratedMethodBuilder)
		{
			CreateWriteLineMessage(decoratedMethodBuilder, "Prefix Action");			
		}
		
		public void PostfixAction(MethodBuilder decoratedMethodBuilder)
		{
			CreateWriteLineMessage(decoratedMethodBuilder, "Postfix Action");			
			ILGenerator decoratedMethodILGenerator = decoratedMethodBuilder.GetILGenerator();
			decoratedMethodILGenerator.Emit(OpCodes.Ret);
		}
		
		public void OuterAction(MethodBuilder decoratedMethodBuilder, MethodInfo innerActionInfo)
		{
			try
			{				
				ILGenerator ilGeneratorForOuterMethod = decoratedMethodBuilder.GetILGenerator();
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				ilGeneratorForOuterMethod.Emit(OpCodes.Ldarg_0);
				ilGeneratorForOuterMethod.Emit(OpCodes.Call, innerActionInfo);
				
				ilGeneratorForOuterMethod.Emit(OpCodes.Nop);
				
				//innerActionInfo.Invoke(instance, new object [] {});
			}				
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
		
		private object fKey = "DecoratorMesssages";
		public object Key
		{
			get
			{
				return fKey;
			}
			
			set
			{
				fKey = value;
			}
		} 
		
		public bool Identify(object key)
		{
			return fKey.Equals(key);
		}
	}
	
}
