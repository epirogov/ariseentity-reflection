
using System;
using System.Reflection;
using System.Reflection.Emit;

namespace EntityReflection
{	
	public interface IDecorateBehaviour
	{		
		void PrefixAction(MethodBuilder decoratedMthodBuilder);		
		void PostfixAction(MethodBuilder decoratedMethodBuilder);		
		void OuterAction(MethodBuilder decoratedMthodBuilder, MethodInfo innerActionInfo);		
	}	
}
