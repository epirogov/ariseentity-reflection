// project created on 21/11/2006 at 09:08
using System;
using PluginInterface;

public class MyClass : PluginInterface.BaseInterface
{
	public static void Method()
	{
		Console.WriteLine("I love you Alenka");
	}
	
	public static void SeccondMethod()
	{
		Console.WriteLine("One I never die and onther too");
	}
	
	private object fKey = "StaticMethodsPlugin"; 
	public object Key
	{
		get
		{
			return fKey;
		}
		
		set
		{
			fKey = value;
		}
	}
		
	public bool Identify(object key)
	{
		return fKey.Equals(key);
	}

}