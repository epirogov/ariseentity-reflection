// created on 11.10.2005 at 21:41

using System;
using System.Collections;
using System.Reflection;
using System.Diagnostics;
using PluginInterface;

namespace PluginCore
{
	public class PluginSearch
	{
		private PluginSearch()
		{
			BindAssembly();
		}
		
		private static PluginSearch fInstance;
		private Assembly fAssembly;
		private string fAssemblyName;
		private string fAssemblySymbolStoreName;
				
		public static PluginSearch Instance
		{
			get
			{
				if (fInstance == null)
					fInstance = new PluginSearch();
				return fInstance;
			}
		
		}
		
		public struct RawAssemblyData
		{
			public byte [] rawAssembly;
			public byte [] rawAssemblySymbolStore;			
		}
		
		public string AssemblyName
		{
			get
			{			
				return fAssemblyName;
			}
			
			set
			{
				fAssemblyName = value;
			}
		}
		
		public string AssemblySymbolStoreName
		{
			get
			{			
				return fAssemblySymbolStoreName;
			}
			
			set
			{
				fAssemblySymbolStoreName = value;
			}
		}
		
		private RawAssemblyData LoadAssembly()
		{
			RawAssemblyData rawData = new RawAssemblyData();					
			rawData.rawAssembly = PluginCore.PluginCrypto.Instance.LoadFile(fAssemblyName);
			rawData.rawAssemblySymbolStore = PluginCore.PluginCrypto.Instance.LoadFile(fAssemblySymbolStoreName);
			return rawData;
		}
		 
		private Assembly AssemblyResolver(object sender, ResolveEventArgs args)
		{
			AppDomain appDomain = (AppDomain)sender;
			RawAssemblyData rawData = LoadAssembly();
			fAssembly = appDomain.Load(rawData.rawAssembly);						
			return fAssembly; 			
		}
		
		private void BindAssembly()
		{
			AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(AssemblyResolver);
		}
		
		public object GetPlugin(object key)
		{
			fAssembly = Assembly.LoadFrom(fAssemblyName);
			if (fAssembly == null)
				throw new System.InvalidOperationException("Assembly not loaded");
			 
			foreach (Module module in fAssembly.GetModules ())
			{
				foreach(Type type in module.GetTypes())
				{
					if (type.GetInterface(typeof(BaseInterface).FullName) != null)
					{		
						object plugin = AppDomain.CurrentDomain.CreateInstance(fAssemblyName, type.FullName).Unwrap();					
						if (((BaseInterface)plugin).Identify(key))
						{
							return plugin;
						}							
					}				
										
				}
			}	
			return null;			
		}
		
		public ArrayList GetPluginKeys()
		{
			ArrayList keys = new ArrayList(); 
			fAssembly = Assembly.LoadFrom(fAssemblyName);
			if (fAssembly == null)
				throw new System.InvalidOperationException("Assembly not loaded");
			 
			foreach (Module module in fAssembly.GetModules ())
			{
				foreach(Type type in module.GetTypes())
				{					
					//Console.WriteLine(type.FullName);
					//Console.WriteLine(type.GetInterface("PluginInterface.BaseInterface"));
					bool isPlugin = false;
					
					//Console.WriteLine("Methods" + Environment.NewLine);
					
					foreach(MethodInfo method in type.GetMethods())
					{
						if (method.Name == "get_Key")
						{
							isPlugin = true;
						}
						
						//Console.WriteLine("Method {0}", method.Name);
					}
					
					//Console.WriteLine("Properties :" + Environment.NewLine);
					
//					foreach(PropertyInfo property in type.GetProperties())
//					{
//						Console.WriteLine("Property {0}", property.Name);
//					}
					
					if (!isPlugin)
					{
//						Console.WriteLine("Type {0} is not plugin", type.FullName);
						continue;
					}
					
					object typeInstance = AppDomain.CurrentDomain.CreateInstance(fAssemblyName, type.FullName).Unwrap();
//					if (!(typeInstance is BaseInterface))
//					{						
//						continue;
//					}
					BaseInterface plugin = (BaseInterface)typeInstance;
					object key = type.InvokeMember("get_Key", BindingFlags.InvokeMethod, null, typeInstance, null);
//					Console.WriteLine("Key {0} :", key);
					keys.Add(key);										
				}
			}	
			return keys;
			
		}
		
		public Type GetPluginType(object key)
		{		 
			
			fAssembly = Assembly.LoadFrom(fAssemblyName);
			if (fAssembly == null)
				throw new System.InvalidOperationException("Assembly not loaded");
				 
			foreach (Module module in fAssembly.GetModules ())
			{
				foreach(Type type in module.GetTypes())
				{				
					//BaseInterface plugin = (BaseInterface)Activator.CreateInstance(type);				
					//BaseInterface plugin = (BaseInterface)AppDomain.CurrentDomain.CreateInstance(fAssemblyName, type.FullName).Unwrap();
												
					bool isPlugin = false;
					foreach (MethodInfo method in type.GetMethods())
					{
						if(method.Name == "get_Key")
						{
							isPlugin = true;
							break;
						}
					}				
						
					if (!isPlugin)
						continue;
						
						
					object typeInstance = Activator.CreateInstance(type);
					if ((bool)type.InvokeMember("Identify", BindingFlags.InvokeMethod, null, typeInstance, new object [] {key})) 
						return type;													
									
										
				}
			}	
			return null;

		} 		
 		
	}
}
