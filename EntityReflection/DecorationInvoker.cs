
using System;
using System.Reflection;
using System.Reflection.Emit;

namespace EntityReflection
{
	
	public class DecorationInvoker
	{
		MethodBuilder initialMethodBuilder;
		IDecorateBehaviour behaviour;
		MethodInfo decoratedMethodInfo;
		
		public DecorationInvoker(MethodBuilder initialMethodBuilder, IDecorateBehaviour behaviour, MethodInfo decoratedMethodInfo)
		{
			this.initialMethodBuilder = initialMethodBuilder;
			this.behaviour = behaviour;
			this.decoratedMethodInfo = decoratedMethodInfo;		
		}
		
		public void InvokeBody()
		{
			behaviour.PrefixAction(initialMethodBuilder);
			behaviour.OuterAction(initialMethodBuilder,decoratedMethodInfo);		
			behaviour.PostfixAction(initialMethodBuilder);
		}
		
		public MethodInfo  GetInvokeBodyInfo()
		{	
			return GetType().GetMethod("InvokeBody");
		}
	}
	
}
