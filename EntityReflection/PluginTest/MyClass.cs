// project created on 11.10.2005 at 22:33
using System;
using PluginInterface;

public class MyClass : PluginInterface.TestInterface
{
	public MyClass()
	{
	}	
	
	private object fKey = "key";
	
	public object Key
	{
		get
		{
			return fKey;
		}	
		set
		{
			fKey = value;
		}
	}
	public bool Identify(object key)
	{
		return fKey.Equals(key);		
	}
	
	public void TestMethod()
	{
	}
}