using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace EntityReflection
{
	
	public class EntityReflection
	{
		
		public EntityReflection()
		{
		}	
		
		private string fDecorAssemblyName = "DecorAssembly.dll";
		public string DecoratedAssemblyName
		{
			get
			{
				return fDecorAssemblyName;
			}
			
			set
			{
				fDecorAssemblyName = value;
			}		
		}
		
		public Type CreateTypeDecoration (Type type, IDecorateBehaviour behaviour)
		{		
			AssemblyName myAssemblyName = new AssemblyName();
			myAssemblyName.Name = "DynamicDecorationAssembly";
			AssemblyBuilder myAssemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(myAssemblyName, AssemblyBuilderAccess.RunAndSave);	 
			ModuleBuilder myModuleBuilder = myAssemblyBuilder.DefineDynamicModule("DynamicDecorationModule", fDecorAssemblyName);
			
			TypeBuilder myTypeBuilder = myModuleBuilder.DefineType(type.Name, TypeAttributes.Public | TypeAttributes.Class);
			myTypeBuilder.SetParent(type);			
			myTypeBuilder.DefineDefaultConstructor(MethodAttributes.Public);
						
			foreach (MethodInfo methodInfo in type.GetMethods())
			{
			    bool isBaseMethod = false;
			    
			    foreach(MethodInfo baseMethodInfo in type.BaseType.GetMethods())
			    {
			    	//Console.WriteLine("BaseMethod {0} is virtual {0}", baseMethodInfo.Name, baseMethodInfo.IsVirtual);
				if (baseMethodInfo.Name == methodInfo.Name)
				{
				    //Console.WriteLine("{0}", methodInfo.Name);
				    isBaseMethod = true;
				    break;
				}
			    }
			    
			    if(isBaseMethod) continue;
			    
			    			
			    //Console.WriteLine("Decorated method : {0}", methodInfo.Name);
			    MethodBuilder methodBuilder = null;
			    if (methodInfo.IsVirtual)
			    {
			    	//Console.WriteLine("Method {0} is virtual	", methodInfo.Name );
			    	
			    	methodBuilder = myTypeBuilder.DefineMethod(methodInfo.Name, MethodAttributes.HideBySig | MethodAttributes.Public | MethodAttributes.ReuseSlot, /*System.Reflection.CallingConvertions.Standard | System.Reflection.CallingConvertions.HasThis, */ methodInfo.ReturnType, new Type [] {});
			    }		    
			    else
			    {
			    	if (!methodInfo.IsStatic)
			    	{
			    		methodBuilder = myTypeBuilder.DefineMethod(methodInfo.Name, MethodAttributes.HideBySig | MethodAttributes.Public | MethodAttributes.ReuseSlot, /*System.Reflection.CallingConvertions.Standard | System.Reflection.CallingConvertions.HasThis, */ methodInfo.ReturnType, new Type [] {});
			    	}
			    	else
			    	{
				    	//Console.WriteLine("Static method {0}", methodInfo.Name);
				    	//methodBuilder = myTypeBuilder.DefineMethod(methodInfo.Name, MethodAttributes.Static | MethodAttributes.Public,  methodInfo.ReturnType, new Type [] {} );
			    	}
			    }			    
			    
			    DecorationInvoker decorationInvoker = new DecorationInvoker(methodBuilder, behaviour, methodInfo);
			    decorationInvoker.InvokeBody();	    			    
			}
			
			Type createdType = myTypeBuilder.CreateType();					 
						
			myAssemblyBuilder.Save("DecoratedAssembly.dll");
			
			Process alprocess = new Process();
			alprocess.StartInfo = new ProcessStartInfo(string.Format("al {0} /out:{0}.manifest", fDecorAssemblyName, fDecorAssemblyName));
			alprocess.Start();
			
			return createdType;
		}
		
		public object CreateInstanceDecoration (Type type, IDecorateBehaviour behaviour)
		{
			return Activator.CreateInstance(CreateTypeDecoration(type, behaviour));
		} 
		
		public void LoadDecoratedAssemblyInCurrentDomain()
		{
		    AssemblyName myAssemblyName = AssemblyName.GetAssemblyName("DecorAssembly.dll");
		    //Console.WriteLine(myAssemblyName.Name);
		    AppDomain.CurrentDomain.Load(myAssemblyName);
		}
	}
	
}
