// created on 11.10.2005 at 21:10

using PluginInterface;
namespace PluginCore
{
	public class PluginFactory
	{
		private PluginFactory()
		{
		}
		
		private static PluginFactory fInstance;
		
		public static PluginFactory Instance
		{
			get
			{
				if (fInstance == null)
					fInstance = new PluginFactory();
				return fInstance;
			}
		
		}		
		 
		public object GetPlugin(object key)
		{			
			return PluginSearch.Instance.GetPlugin(key);
		}			
	}
}