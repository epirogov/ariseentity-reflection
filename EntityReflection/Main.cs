// project created on 02.05.2006 at 17:05
using System;
using System.Reflection;
using System.Collections;
using PluginInterface;
using PluginCore;

namespace EntityReflection
{
	class MainClass
	{
		private static string fAssemblyName = "DecorateEntities";
		public static void Main(string[] args)
		{			
			Console.WriteLine("Entry Reflection for Alenka and Kostya\n");
			
			PluginSearch.Instance.AssemblyName=  fAssemblyName + ".dll";
			PluginSearch.Instance.AssemblySymbolStoreName = fAssemblyName + ".dll.mdb";
			
			//Console.WriteLine("Keys :");
			System.Collections.ArrayList keys = new System.Collections.ArrayList();
			foreach (string key in PluginSearch.Instance.GetPluginKeys())
			{
				//Console.WriteLine(key);
				keys.Add(key);
			}
			
			foreach (object key in keys)
			{ 
				Type pluginType = PluginSearch.Instance.GetPluginType((string)key);
				if (pluginType == null)
				{
					Console.WriteLine("Plugin not found");
					return;
				}
				
				
				//BaseInterface myinstances = (BaseInterface)PluginSearch.Instance.GetPlugin((string)key);
				
				Type Decorator = PluginSearch.Instance.GetPluginType("DecoratorMesssages");
				
				IDecorateBehaviour decorationInstance = (IDecorateBehaviour)Activator.CreateInstance(Decorator);
				
				
				//DecorateBehavior b = new DecorateBehavior(); 
				EntityReflection e = new EntityReflection();
				
				Type myDecoratedType = e.CreateTypeDecoration(typeof(ClassA),decorationInstance);
				//e.LoadDecoratedAssemblyInCurrentDomain();
				Console.WriteLine("\n Generated Class : {0}\n",myDecoratedType.FullName);
				
	
				object decoratedObject = Activator.CreateInstance(myDecoratedType, true);
												
				decoratedObject.GetType().InvokeMember("Method",BindingFlags.InvokeMethod, null, decoratedObject, null );				
			}
		}
	}
}