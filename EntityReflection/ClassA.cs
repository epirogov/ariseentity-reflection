
using System;

namespace EntityReflection
{
	
	public class ClassA
	{
		
		public ClassA()
		{
		}
		
		public virtual void Method()
		{
			Console.WriteLine("Class A method");
			
		}
	}
	
}
